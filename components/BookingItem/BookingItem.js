import { View, Text, StyleSheet } from "react-native";

function BookingItem(props) {
  return (
    <View id="props.item.id" style={styles.itemContainer}>
      <View
        style={{
          flexDirection: "row",
        }}
      >
        <Text style={styles.bookingHeading}>
          Room no: {props.item.roomNumber}
        </Text>
        <Text style={styles.bookingHeading}>{props.item.name}</Text>
        <Text style={styles.bookingHeading}>{props.item.place}</Text>
      </View>
      <Text>{props.item.phoneNumber}</Text>
      <Text>{props.item.noOfPeople}</Text>
      <Text>{props.item.checkinDate}</Text>
      <Text>{props.item.estimatedCheckoutDate}</Text>
    </View>
  );
}

export default BookingItem;

const styles = StyleSheet.create({
  itemContainer: {
    backgroundColor: "#cccccc",
    flexDirection: "column",
    justifyContent: "space-between",
    marginTop: 8,
    padding: 8,
  },
  bookingHeading: {
    fontSize: 20,
    marginRight: 24,
  },
});
