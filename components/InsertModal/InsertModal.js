import { useState } from "react";
import {
  Modal,
  View,
  TextInput,
  Text,
  StyleSheet,
  Button,
  ScrollView,
} from "react-native";
import { DateTimePickerAndroid } from "@react-native-community/datetimepicker";
import { Booking } from "../../models/booking";

function InsertModal(props) {
  const [name, setName] = useState();
  const [place, setPlace] = useState();
  const [phoneNumber, setPhoneNumber] = useState();
  const [noOfPeople, setNoOfPeople] = useState();
  const [checkinDate, setCheckinDate] = useState(new Date());
  const [checkoutDate, setCheckoutDate] = useState(new Date());
  const [roomNumber, setRoomNumber] = useState();
  const [rate, setRate] = useState();

  const onCheckinChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    setCheckinDate(currentDate);
  };

  const onCheckoutChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    setCheckoutDate(currentDate);
  };

  const showMode = (currentMode, value, onChange) => {
    DateTimePickerAndroid.open({
      value,
      onChange,
      mode: currentMode,
      is24Hour: true,
    });
  };

  const showDatepicker = (value, onChange) => {
    showMode("date", value, onChange);
  };

  const showTimepicker = (value, onChange) => {
    showMode("time", value, onChange);
  };

  function resetFormData() {
    setName("");
    setPlace("");
    setPhoneNumber("");
    setNoOfPeople("");
    setCheckinDate(new Date());
    setCheckoutDate(new Date());
    setRoomNumber("");
    setRate("");
  }

  function onSubmitHandler() {
    const booking = new Booking(
      name,
      place,
      phoneNumber,
      noOfPeople,
      checkinDate.toISOString(),
      checkoutDate.toISOString(),
      rate,
      roomNumber
    );
    props.onSubmit(booking);
    resetFormData();
  }

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={props.show}
      onRequestClose={() => {
        props.onHide();
        resetFormData();
      }}
    >
      <ScrollView>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalTitle}>Enter new check in details</Text>
            <View
              style={{
                width: "100%",
                justifyContent: "center",
                alignContent: "center",
              }}
            >
              <Text style={styles.inputLabel}>Name:</Text>
              <TextInput
                style={styles.textInput}
                placeholder="Name"
                value={name}
                onChangeText={(value) => setName(value)}
              />
              <Text style={styles.inputLabel}>Place:</Text>
              <TextInput
                style={styles.textInput}
                placeholder="Place"
                value={place}
                onChangeText={(value) => setPlace(value)}
              />
              <Text style={styles.inputLabel}>Phone number:</Text>
              <TextInput
                style={styles.textInput}
                placeholder="Phone number"
                inputMode="tel"
                value={phoneNumber}
                onChangeText={(value) => setPhoneNumber(value)}
              />
              <Text style={styles.inputLabel}>No. of people:</Text>
              <TextInput
                style={styles.textInput}
                placeholder="No. of people"
                inputMode="numeric"
                value={noOfPeople}
                onChangeText={(value) => setNoOfPeople(value)}
              />
              <View style={{ marginVertical: 8 }}>
                <Text style={styles.inputLabel}>Check in date:</Text>
                <Text style={styles.inputDate}>
                  {checkinDate.toLocaleString()}
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Button
                    onPress={() => showDatepicker(checkinDate, onCheckinChange)}
                    title="Ch.Date"
                  />
                  <Button
                    onPress={() => showTimepicker(checkinDate, onCheckinChange)}
                    title="Ch.Time"
                  />
                  <Button
                    onPress={() => setCheckinDate(new Date())}
                    title="now"
                  />
                </View>
              </View>
              <View style={{ marginVertical: 8 }}>
                <Text style={styles.inputLabel}>Estimated checkout date:</Text>
                <Text style={styles.inputDate}>
                  {checkoutDate.toLocaleString()}
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Button
                    onPress={() =>
                      showDatepicker(checkoutDate, onCheckoutChange)
                    }
                    title="Ch.Date"
                  />
                  <Button
                    onPress={() =>
                      showTimepicker(checkoutDate, onCheckoutChange)
                    }
                    title="Ch.Time"
                  />
                  <Button
                    onPress={() => {
                      const currentDate = new Date();
                      const tomorrowDate = new Date(
                        currentDate.setDate(currentDate.getDate() + 1)
                      );
                      setCheckoutDate(tomorrowDate);
                    }}
                    title="24hrs"
                  />
                </View>
              </View>
              <Text style={styles.inputLabel}>Room number:</Text>
              <TextInput
                style={styles.textInput}
                placeholder="Room number"
                value={roomNumber}
                onChangeText={(value) => setRoomNumber(value)}
              />
              <Text style={styles.inputLabel}>Rate:</Text>
              <TextInput
                style={styles.textInput}
                placeholder="Rate"
                value={rate}
                onChangeText={(value) => setRate(value)}
                inputMode="numeric"
              />
            </View>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-evenly",
                width: "100%",
              }}
            >
              <Button onPress={onSubmitHandler} title="Confirm" color="green" />
            </View>
          </View>
        </View>
      </ScrollView>
    </Modal>
  );
}

export default InsertModal;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 0,
    width: "100%",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: "100%",
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalTitle: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 20,
  },
  textInput: {
    borderWidth: 1,
    borderColor: "#cccccc",
    marginVertical: 8,
    padding: 8,
    fontSize: 16,
  },
  inputLabel: {
    fontSize: 16,
  },
  inputDate: {
    fontSize: 20,
  },
});
