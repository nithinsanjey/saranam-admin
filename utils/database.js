import * as SQLite from "expo-sqlite";
import { Booking } from "../models/booking";

const database = SQLite.openDatabase("bookings.db");

export function init() {
  const promise = new Promise((resolve, reject) => {
    database.transaction((tx) => {
      tx.executeSql(
        `CREATE TABLE IF NOT EXISTS bookings (
                    id INTEGER PRIMARY KEY NOT NULL,
                    name TEXT NOT NULL,
                    place TEXT NOT NULL,
                    phone_number TEXT NOT NULL,
                    no_of_people TEXT NOT NULL,
                    checkin_date DATE NOT NULL,
                    estimated_checkout_date DATE NOT NULL,
                    rate TEXT NOT NULL,
                    room_number TEXT NOT NULL,
                    actual_checkout_date DATE
                    )`,
        [],
        () => {
          resolve();
        },
        (_, error) => {
          reject(error);
        }
      );
    });
  });
  return promise;
}

export function insertBooking(booking) {
  const promise = new Promise((resolve, reject) => {
    database.transaction((tx) => {
      tx.executeSql(
        `INSERT INTO bookings (
              name,
              place,
              phone_number,
              no_of_people,
              checkin_date,
              estimated_checkout_date,
              rate,
              room_number,
              actual_checkout_date
              ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`,
        [
          booking.name,
          booking.place,
          booking.phoneNumber,
          booking.noOfPeople,
          booking.checkinDate,
          booking.estimatedCheckoutDate,
          booking.rate,
          booking.roomNumber,
          booking.actualCheckoutDate,
        ],
        (_, result) => {
          console.log(result);
          resolve(result);
        },
        (_, error) => {
          console.log(error);
          reject(error);
        }
      );
    });
  });

  return promise;
}

export function fetchBookings() {
  const promise = new Promise((resolve, reject) => {
    database.transaction((tx) => {
      tx.executeSql(
        `SELECT * FROM bookings`,
        [],
        (_, result) => {
          console.log(result.rows._array);
          const bookings = [];

          for (const dp of result.rows._array) {
            bookings.push(
              new Booking(
                dp.name,
                dp.place,
                dp.phone_number,
                dp.no_of_people,
                dp.checkin_date,
                dp.estimated_checkout_date,
                dp.rate,
                dp.room_number
              )
            );
          }

          resolve(bookings);
        },
        (_, error) => {
          console.log(error);
          reject(error);
        }
      );
    });
  });
  return promise;
}

function dropTableBookings() {
  //   tx.executeSql(
  //     `DROP TABLE bookings`,
  //     [],
  //     (_, result) => {
  //       console.log(result);
  //       resolve(result);
  //     },
  //     (_, error) => {
  //       console.log(error);
  //       reject(error);
  //     }
  //   );
}
