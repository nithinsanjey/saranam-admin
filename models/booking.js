export class Booking {
  constructor(
    name,
    place,
    phoneNumber,
    noOfPeople,
    checkinDate,
    estimatedCheckoutDate,
    rate,
    roomNumber,
    id
  ) {
    this.name = name;
    this.place = place;
    this.phoneNumber = phoneNumber;
    this.noOfPeople = noOfPeople;
    this.checkinDate = checkinDate;
    this.estimatedCheckoutDate = estimatedCheckoutDate;
    this.rate = rate;
    this.roomNumber = roomNumber;
    this.actualCheckoutDate = null;
    this.id = id;
  }
}
