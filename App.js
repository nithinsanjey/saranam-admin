import { useEffect, useState, useCallback } from "react";
import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  Button,
  ScrollView,
  Alert,
} from "react-native";
import InsertModal from "./components/InsertModal/InsertModal";
import { fetchBookings, init, insertBooking } from "./utils/database";
import * as SplashScreen from "expo-splash-screen";
import BookingItem from "./components/BookingItem/BookingItem";

export default function App() {
  const [modalVisible, setModalVisible] = useState(false);
  const [dbInitialized, setDbInitialized] = useState(false);
  const [allBookings, setAllBookings] = useState([]);

  useEffect(() => {
    async function prepare() {
      await SplashScreen.preventAutoHideAsync();
      init()
        .then(() => {
          setDbInitialized(true);
        })
        .catch((error) => {
          console.log(error);
        });
      const bookings = await fetchBookings();
      setAllBookings(bookings);
    }
    prepare();
  }, []);

  useEffect(() => {
    async function fetchAllBookings() {
      const bookings = await fetchBookings();
      setAllBookings(bookings);
    }
    fetchAllBookings();
  }, [modalVisible]);

  const onLayoutRootView = useCallback(async () => {
    if (dbInitialized) {
      // This tells the splash screen to hide immediately! If we call this after
      // `setAppIsReady`, then we may see a blank screen while the app is
      // loading its initial state and rendering its first pixels. So instead,
      // we hide the splash screen once we know the root view has already
      // performed layout.
      await SplashScreen.hideAsync();
    }
  }, [dbInitialized]);

  async function onSubmitHandler(booking) {
    await insertBooking(booking);
    Alert.alert("Booking saved!");
    setModalVisible(false);
  }

  if (!dbInitialized) {
    return null;
  }

  return (
    <View style={styles.container} onLayout={onLayoutRootView}>
      <ScrollView>
        <InsertModal
          show={modalVisible}
          onHide={() => setModalVisible(false)}
          onSubmit={onSubmitHandler}
        />
        <View style={styles.header}>
          <Text style={styles.headerText}>Saranam Admin</Text>
          <Button
            title="+new"
            color="green"
            onPress={() => setModalVisible(true)}
          />
        </View>
        <View style={{ marginTop: 24 }}>
          {allBookings.map((booking) => {
            return <BookingItem item={booking} />;
          })}
        </View>
      </ScrollView>
      <StatusBar style="auto" backgroundColor="white" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    margin: 8,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "center",
    marginTop: 32,
  },
  headerText: {
    fontSize: 24,
  },
});
